
## Useful links:
 * [npm mysql](https://www.npmjs.com/package/mysql)
 * [meteor quick tutorial](https://medium.com/@tkssharma/my-story-about-meteor-development-app-in-just-4-hours-with-learning-f1d203ce3444)
 * [meteor with docker](https://forums.meteor.com/t/running-meteor-on-docker/46025)
 * [meteor docker launchpad](https://github.com/jshimko/meteor-launchpad)