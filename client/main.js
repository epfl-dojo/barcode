import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';


function eventuallyPrefill() {
  TESTDATA=`# ---------- test data
# CAMIPRO
E0952884
# Emplacement
S.13
# Produit
00415660
222222222
00415682
S.20
00415659
00415661
00415602
# Emplacement en double (erreur)
S.15
S.15
00282874
00415602
00415661
# Produit aussi
9999-9999    
`;
  ta=$("textarea");
  if (ta.val() == '') {
    ta.val(TESTDATA);
    return true;
  } else {
    return false;
  }
}

function contactServer() {
  console.log("contactServer enter");
  Meteor.call('productByBarcode', "00415659", function(error, result) {
    if (error) {
      console.log('Meteor.call(productByBarcode): error:', error);
    } else {
      console.log('Meteor.call(productByBarcode): result:', result);
    }
  });
  console.log("contactServer exit");
}

function updateBarcodes() {
  console.log("updateBarcodes: enter");
  contactServer();
  console.log("updateBarcodes: exit");
}


Template.userinput.events({
  'click button' (event, instance) {
    if (eventuallyPrefill()){return;}
    updateBarcodes();
  } 
})


Template.barcodes.onCreated(function helloOnCreated() {
  console.log("barcodes.onCreated: enter");
  this.barcodes = new ReactiveVar([]);
  console.log("barcodes.onCreated: exit");
});


Template.barcodes.helpers({
  getBarcodes() {
    let barcodes = Template.instance().barcodes.get();  // TODO : ameliorer
    // return barcodes.map(analyseBarcode)
    asyncAnalyseBarcode
  }
});


Template.barcode.onCreated(function () {
  console.log("barcode.onCreated: enter");
  console.log("barcode.onCreated: exit");
});

// Template.barcode.events({
//   'click button'(event, instance) {
//     contactServer();
//   },

//   'keyup textarea' (event, instance) {
//     console.log("textarea updated")
//     instance.barcodes.set(barcodeLines());
//   }
// });


function barcodeLines() {
  let content = $("textarea").val()
  return content.split("\n")
}

function analyseBarcode (barcode) {
  if (barcode.match(/^E/)){
    return {text:barcode, type:"Camipro"}
  } else if (barcode.match(/^[0-9]/)){
    return {text:barcode, type:"Produit"}
  } else if (barcode.match(/^S/)){
    return {text:barcode, type:"Local"}
  } else {
    return {barcode:barcode, type:"Unknown"}
  }
}