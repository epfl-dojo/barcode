import { Meteor } from 'meteor/meteor';



var mysql = require('mysql');
var db = mysql.createPool({
  connectionLimit : 1,
  host     : 'db',
  user     : 'root',
  password : 'ROOT',
  database : 'barcode'
});
// db.end()




// var queryProductByBarcode = function(barcode, callback) {
//   console.log("queryProductByBarcode: enter")
//   var query = 'SELECT * FROM `products` where `barcode` = ?';
//   // connection is implicitly established
//   db.query(query, [barcode], function(err, rows, fields) {
//     if (err) throw err;
//     console.log("queryProductByBarcode: done query")
//     callback(null, {result: rows[0]});
//   });
//   console.log("queryProductByBarcode: exit")
// }

// var productByBarcode = Meteor.wrapAsync(queryProductByBarcode);

// Meteor.methods({
//   productByBarcode: function (barcode) {
//     return productByBarcode(barcode);
//   }
// });




var queryDataByBarcode = function(table, barcode, callback) {
  console.log("queryDataByBarcode: enter")
  var query = 'SELECT * FROM `products` where `barcode` = ?';
  // connection is implicitly established
  db.query(query, [barcode], function(err, rows, fields) {
    if (err) throw err;
    console.log("queryDataByBarcode: done query")
    callback(null, {result: rows[0]});
  });
  console.log("queryDataByBarcode: exit")
}

var dataByBarcode = Meteor.wrapAsync(queryDataByBarcode);

Meteor.methods({
  productByBarcode: function (barcode) {
    return dataByBarcode("products", barcode);
  },
  locationByBarcode: function (barcode) {
    return dataByBarcode("location", barcode);
  },
  employeeByBarcode: function (barcode) {
    return dataByBarcode("employees", barcode);
  },
});







Meteor.startup(() => {
  // code to run on server at startup

  var connection = mysql.createConnection({
      host     : 'db',
      user     : 'root',
      password : 'ROOT',
      database : 'barcode'
  });

  // connection.connect();
  // connection.query('SELECT * FROM employees', function(err, rows, fields) {
  //   if (err) throw err;
  //   console.log(rows);
  // });
  // connection.query('SELECT * FROM locations', function(err, rows, fields) {
  //   if (err) throw err;
  //   console.log(rows);
  // });
  // connection.query('SELECT * FROM products', function(err, rows, fields) {
  //   if (err) throw err;
  //   console.log(rows);
  // });
  // connection.end();

});
