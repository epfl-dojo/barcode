.PHONY: down logs meteor ps reload seed shell up

up:
	docker-compose up -d

down:
	docker-compose down

reload:
	docker-compose stop app
	docker-compose up -d

logs:
	docker-compose logs -f

ps:
	docker-compose ps

shell: up
	docker-compose exec app bash

meteor:
	docker-compose run --entrypoint bash app

seed: up seed.sql
	echo "CREATE OR REPLACE DATABASE barcode;" | docker-compose exec -T db mysql -u root --password=ROOT 
	docker-compose exec -T db mysql -u root --password=ROOT barcode < seed.sql 
